var searchData=
[
  ['logfailure',['logFailure',['../logger_8c.html#afbda01404bd2cf70cb7834e97f062d02',1,'logFailure(FILE *logFile, char *player, char id):&#160;logger.c'],['../logger_8h.html#afbda01404bd2cf70cb7834e97f062d02',1,'logFailure(FILE *logFile, char *player, char id):&#160;logger.c']]],
  ['loggameover',['logGameOver',['../logger_8c.html#a3de46b72357b5974ffde0770546ab7aa',1,'logGameOver(FILE *logFile, char *p1, int score1, char *p2, int score2):&#160;logger.c'],['../logger_8h.html#a3de46b72357b5974ffde0770546ab7aa',1,'logGameOver(FILE *logFile, char *p1, int score1, char *p2, int score2):&#160;logger.c']]],
  ['logger_2ec',['logger.c',['../logger_8c.html',1,'']]],
  ['logger_2eh',['logger.h',['../logger_8h.html',1,'']]],
  ['logstart',['logStart',['../logger_8c.html#a37df9ced31212503fdf7c15068506640',1,'logStart(FILE *logFile, char *p1, char *p2):&#160;logger.c'],['../logger_8h.html#a37df9ced31212503fdf7c15068506640',1,'logStart(FILE *logFile, char *p1, char *p2):&#160;logger.c']]],
  ['logtimeout',['logTimeout',['../logger_8c.html#adc6a6428a98ce03d62a323ef8a0bc03b',1,'logTimeout(FILE *logFile, char *player, char id):&#160;logger.c'],['../logger_8h.html#adc6a6428a98ce03d62a323ef8a0bc03b',1,'logTimeout(FILE *logFile, char *player, char id):&#160;logger.c']]],
  ['logturn',['logTurn',['../logger_8c.html#a389f3f437594fd7df28618a1be6c307a',1,'logTurn(FILE *logFile, GameAction action):&#160;logger.c'],['../logger_8h.html#a389f3f437594fd7df28618a1be6c307a',1,'logTurn(FILE *logFile, GameAction action):&#160;logger.c']]]
];
