# Specifications

Ce projet est divisé en plusieurs modules
- Serveur de partie
- Interprétation d'entrées
- Affichage
- Boucle Principale
- IA

## Serveur
 - Gère partie
 - Répond aux commandes
 - Vérifie les légalités des commandes
 - Détecte les fins de parties

## Gestion IHM
 - Graphique Clavier/Sourie
 - Terminal Lire entrées
 - Envoyer commandes

## IA
 - Lire plateau
 - Trouver les mouvements légaux
 - Choisir le meilleur mouvement
 - Envoyer commandes

## Boucle Principale
 - Execute le serveur
 - Execute l'affichage et attends une entrée
 - Executer les tours de l'IA
