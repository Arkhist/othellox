# Besoins du projet

- Jeu d'Othello

Jouer contre autre joueur
Détection de fin de partie
Gestion du plateau
Tour par tour
Possibilité de rejouer

- Interface terminal

Lancement du jeu en commande
Selection des IA

- Interface graphique

Menu
Sélection du mode de jeu
Sélection des IA
Graphismes
Interactions

Solution particulière : *SDL*

- Gestion IA

IA intelligente, interdiction d'une IA purement aléatoire.
Possibilité d'intégrer plusieurs IA externes

Solution particulière : *Utilisation d'IA externe en .so*


Dépendances du projet :
- SDL
