#pragma once

#include "ai_api.h"

struct GameNodeS;

typedef struct GameNodeS
{
    Vec2_c link;
    TileType player;
    TileType board[8][8];

    int childrenAmt;
    Vec2_c availableMoves[64];
    struct GameNodeS** children;

    int score;
} GameNode;

GameNode* createNodeFromState(struct ai_boardInter inter, GameState* state, char pId);
GameNode* createNodeFromLink(struct ai_boardInter inter, GameNode* node, int move);

void freeGameNode(GameNode* node);
void freeGameNodeUpTo(struct ai_boardInter boardInter, GameNode* node, GameState* find, /*@out@*/ GameNode** output);
void freeGameNodeUpToChild(GameNode* node, GameNode* child);

int getHeuristicScore(GameNode* node, TileType forPlayer);
int setHeuristicScore(GameNode* node, TileType forPlayer);
