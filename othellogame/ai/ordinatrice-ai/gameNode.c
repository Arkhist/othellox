#include "gameNode.h"

#include <stdlib.h>


GameNode* createNodeFromState(struct ai_boardInter inter, GameState* state, char pid)
{
    GameNode* r = (GameNode*)calloc(1, sizeof(GameNode));
    char valid = 0;
    if(r != NULL)
    {
        r->player = pid;
        Vec2_c moves[64];
        int numMoves = 0;
        int score = 0;
        TileType stateBoard[8][8];
        inter.getBoard(state, stateBoard);
        for(int i = 0; i < 64; i++)
        {
            r->board[i/8][i%8] = stateBoard[i/8][i%8];

            if(stateBoard[i/8][i%8] != T_EMPTY)
                continue;
            valid = 0;
            Neighbor neighbors[8];
            inter.getValidNeighbors(stateBoard, pid, neighbors, i/8, i%8);
            for(int j = 0; j < 8; j++)
            {
                if(neighbors[j].posX != -1)
                {
                    valid = 1;
                    break;
                }
            }
            if(valid == 1)
                moves[numMoves++] = (Vec2_c){i/8, i%8};

            TileType cur = inter.getTile(state, i/8, i%8);
            score += ((cur == pid) ? 1 : ((cur == T_EMPTY) ? 0 : -1));
        }
        r->children = (GameNode**)calloc(numMoves, sizeof(GameNode*));
        for(int i = 0; i < 64; i++)
        {
            if(i < numMoves)
            {
                r->children[i] = NULL;
                r->availableMoves[i] = moves[i];
            }
            else
                r->availableMoves[i] = (Vec2_c){-1, -1};
        }
        r->childrenAmt = numMoves;
        r->link = (Vec2_c){-1, -1};
        r->score = score;
    }
    return r;
}

GameNode* createNodeFromLink(struct ai_boardInter inter, GameNode* node, int move, char pid)
{
    GameNode* r = (GameNode*)calloc(1, sizeof(GameNode));
    char valid = 0;
    if(r != NULL)
    {
        r->player = (node->player+1)%2;
        for(int i = 0; i < 64; i++)
            r->board[i/8][i%8] = node->board[i/8][i%8];
        
        // Executing the move
        Vec2_c linkMove = node->availableMoves[move];
        Neighbor neighbors[8];
        inter.getValidNeighbors(r->board, node->player, neighbors, linkMove.posY, linkMove.posX);
        r->board[(int)linkMove.posY][(int)linkMove.posX] = node->player;
        for(int i = 0; i < 8; i++)
        {
            if(neighbors[i].posX == -1)
                continue;
            char curX = neighbors[i].posX;
            char curY = neighbors[i].posY;
            
            while(r->board[(int)curY][(int)curX] == r->player)
            {
                r->board[(int)curY][(int)curX] = node->player;
                curX += neighbors[i].relX;
                curY += neighbors[i].relY;
            }
        }
        // Move executed

        Vec2_c moves[64];
        int numMoves = 0;
        int score = 0;
        for(int i = 0; i < 64; i++)
        {
            TileType cur = r->board[i/8][i%8];
            int modifier = 1;
            if(i/8 == 7 || i/8 == 0 || i%8 == 0 || i%8 == 7)
                modifier = 4;
            score += ((cur == pid) ? 1 : ((cur == T_EMPTY) ? 0 : -1));

            if(r->board[i/8][i%8] != T_EMPTY)
                continue;
            valid = 0;
            Neighbor neighbors[8];
            inter.getValidNeighbors(r->board, r->player, neighbors, i/8, i%8);
            for(int j = 0; j < 8; j++)
            {
                if(neighbors[j].posX != -1)
                {
                    valid = 1;
                    break;
                }
            }
            if(valid == 1)
                moves[numMoves++] = (Vec2_c){i/8, i%8};
        }
        r->children = (GameNode**)calloc(numMoves, sizeof(GameNode*));
        for(int i = 0; i < 64; i++)
        {
            if(i < numMoves)
            {
                r->children[i] = NULL;
                r->availableMoves[i] = moves[i];
            }
            else
                r->availableMoves[i] = (Vec2_c){-1, -1};
        }
        r->childrenAmt = numMoves;
        r->link = linkMove;
        r->score = score;
    }
    return r;
}

void freeGameNode(GameNode* node)
{
    for(int i = 0; i < node->childrenAmt; i++)
    {
        if(node->children[i] != NULL)
            freeGameNode(node->children[i]);
    }
    if(node->childrenAmt > 0)
        free(node->children);
    free(node);
}
