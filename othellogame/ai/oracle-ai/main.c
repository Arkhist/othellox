#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "ai_api.h"

#include "gameNode.h"

#define THRESHOLD 0.975



float max(float a, float b)
{
    return (a > b) ? a : b;
}

float min(float a, float b)
{
    return (a > b) ? b : a;
}

struct ai_inter interface;

struct ai_boardInter boardInter;
struct ai_actionInter actionInter;


GameNode* savedNode = NULL;


void init(struct ai_inter inter)
{
    interface = inter;
    boardInter = inter.boardInter;
    actionInter = inter.actionInter;
}

clock_t startTurn;

float getElapsedTime()
{
    return (float)(clock()-startTurn)/((float)CLOCKS_PER_SEC);
}

GameNode* a;
TileType sortFor;
int compMoves(const void* move1, const void* move2)
{
    int move1id = *((int*)move1);
    int move2id = *((int*)move2);
    if(a == NULL)
        return 0;
    if(a->children[move1id] == NULL)
        return 1;
    if(a->children[move2id] == NULL)
        return 0;
    if(a->children[move1id]->value[sortFor+2] > a->children[move2id]->value[sortFor+2])
        return -1;
    else if(a->children[move1id]->value[sortFor+2] < a->children[move2id]->value[sortFor+2])
        return 1;
    return 0;
}

float alphaBeta(GameNode* node, int depth, TileType forPlayer, TileType aiPlayer, float alpha, float beta)
{
    a = node;
    sortFor = aiPlayer;

    if(getElapsedTime() > THRESHOLD)
        return 1;
    if(depth <= 0 || node->childrenAmt == 0)
    {
        node->value[aiPlayer+2] = getHeuristicScore(boardInter, node, aiPlayer);
        return node->value[aiPlayer+2];
    }
    

    int arr[node->childrenAmt];
    for(int i = 0; i < node->childrenAmt; i++)
        arr[i] = i;
    qsort(arr, node->childrenAmt, sizeof(int), compMoves);

    float value;
    if(forPlayer == aiPlayer)
    {
        value = nINF;
        for(int i = 0; i < node->childrenAmt; i++)
        {
            int index = arr[i];
            if(node->children[index] == NULL)
                node->children[index] = createNodeFromLink(boardInter, node, index);
            value = max(value, alphaBeta(node->children[index], depth - 1, (forPlayer+1)%2, aiPlayer, alpha, beta));
            alpha = max(alpha, value);
            if(alpha >= beta)
                break;
        }
    }
    else
    {
        value = pINF;
        for(int i = 0; i < node->childrenAmt; i++)
        {
            int index = arr[i];
            if(node->children[index] == NULL)
                node->children[index] = createNodeFromLink(boardInter, node, index);
            value = min(value, alphaBeta(node->children[index], depth - 1, (forPlayer+1)%2, aiPlayer, alpha, beta));
            beta = min(beta, value);
            if(alpha >= beta)
                break;
        }
    }
    node->value[aiPlayer+2] = value;
    return value;
}

float mtdf(GameNode* root, float guessArbitrary, int depth, TileType forPlayer)
{
    float guess = guessArbitrary;
    float upper = pINF;
    float lower = nINF;
    while(lower < upper)
    {
        float beta = max(guess, lower+1);
        guess = alphaBeta(root, depth, forPlayer, forPlayer, beta-1, beta);
        if(getElapsedTime() > THRESHOLD)
            return 1;
        if(guess < beta)
            upper = guess;
        else
            lower = guess;
    }
    return guess;
}

int powi(int v, int e)
{
    if(e == 0)
        return 1;
    if(e == 1)
        return v;
    if(e % 2 == 1)
        return powi(v, e/2) * powi(v, e/2) * v;
    else
        return powi(v, e/2) * powi(v, e/2);
}

void runTurn(GameState* state, char pId)
{
    startTurn = clock();
    GameNode* root = NULL;
    freeGameNodeUpTo(boardInter, savedNode, state, &root);
    if(root == NULL)
        root = createNodeFromState(boardInter, state, pId);
    savedNode = NULL;
    float guess = 31;
    int i;
    for(i = 3; i < 12; i++)
    {
        guess = mtdf(root, guess, i, pId);
        if(getElapsedTime() > THRESHOLD)
            break;
        for(int j = 0; j < root->childrenAmt; j++)
        {
            if(root->children[j] == NULL)
                continue;
            root->children[j]->value[0] = root->children[j]->value[2];
            root->children[j]->value[1] = root->children[j]->value[3];
        }
    }
    int value = nINF;
    int maxInd = -1;
    for(int i = 0; i < root->childrenAmt; i++)
    {
        if(root->children[i] == NULL)
            continue;
        if(root->children[i]->value[pId] > value)
        {
            value = root->children[i]->value[pId];
            savedNode = root->children[i];
            maxInd = i;
        }
    }
    Vec2_c target = root->availableMoves[maxInd];

    int valid = actionInter.placeStone(state, target.posY, target.posX);
    valid++;

    if(savedNode != NULL)
        freeGameNodeUpToChild(root, savedNode);
    else
        freeGameNode(root);
}

void destroy()
{
    freeGameNode(savedNode);
}
