#include "gameNode.h"

#include <stdlib.h>


GameNode* createNodeFromState(struct ai_boardInter inter, GameState* state, char pid)
{
    GameNode* r = (GameNode*)calloc(1, sizeof(GameNode));
    char valid = 0;
    if(r != NULL)
    {
        r->player = pid;
        Vec2_c moves[64];
        int numMoves = 0;
        TileType stateBoard[8][8];
        inter.getBoard(state, stateBoard);
        for(int i = 0; i < 64; i++)
        {
            r->board[i/8][i%8] = stateBoard[i/8][i%8];

            if(stateBoard[i/8][i%8] != T_EMPTY)
                continue;
            valid = 0;
            Neighbor neighbors[8];
            inter.getValidNeighbors(stateBoard, pid, neighbors, i/8, i%8);
            for(int j = 0; j < 8; j++)
            {
                if(neighbors[j].posX != -1)
                {
                    valid = 1;
                    break;
                }
            }
            if(valid == 1)
                moves[numMoves++] = (Vec2_c){i/8, i%8};
        }
        r->children = (GameNode**)calloc(numMoves, sizeof(GameNode*));
        for(int i = 0; i < 64; i++)
        {
            if(i < numMoves)
            {
                r->children[i] = NULL;
                r->availableMoves[i] = moves[i];
            }
            else
                r->availableMoves[i] = (Vec2_c){-1, -1};
        }
        r->childrenAmt = numMoves;
        r->link = (Vec2_c){-1, -1};
        for(int i = 0; i < 4; i++)
            r->value[i] = nINF;
        r->score[0] = nINF; 
        r->score[1] = nINF;
    }
    return r;
}

GameNode* createNodeFromLink(struct ai_boardInter inter, GameNode* node, int move)
{
    GameNode* r = (GameNode*)calloc(1, sizeof(GameNode));
    char valid = 0;
    if(r != NULL)
    {
        r->player = (node->player+1)%2;
        for(int i = 0; i < 64; i++)
            r->board[i/8][i%8] = node->board[i/8][i%8];
        
        // Executing the move
        Vec2_c linkMove = node->availableMoves[move];
        Neighbor neighbors[8];
        inter.getValidNeighbors(r->board, node->player, neighbors, linkMove.posY, linkMove.posX);
        r->board[(int)linkMove.posY][(int)linkMove.posX] = node->player;
        for(int i = 0; i < 8; i++)
        {
            if(neighbors[i].posX == -1)
                continue;
            char curX = neighbors[i].posX;
            char curY = neighbors[i].posY;
            
            while(r->board[(int)curY][(int)curX] == r->player)
            {
                r->board[(int)curY][(int)curX] = node->player;
                curX += neighbors[i].relX;
                curY += neighbors[i].relY;
            }
        }
        // Move executed

        Vec2_c moves[64];
        int numMoves = 0;
        for(int i = 0; i < 64; i++)
        {
            if(r->board[i/8][i%8] != T_EMPTY)
                continue;
            valid = 0;
            Neighbor neighbors[8];
            inter.getValidNeighbors(r->board, r->player, neighbors, i/8, i%8);
            for(int j = 0; j < 8; j++)
            {
                if(neighbors[j].posX != -1)
                {
                    valid = 1;
                    break;
                }
            }
            if(valid == 1)
                moves[numMoves++] = (Vec2_c){i/8, i%8};
        }
        r->children = (GameNode**)calloc(numMoves, sizeof(GameNode*));
        for(int i = 0; i < 64; i++)
        {
            if(i < numMoves)
            {
                r->children[i] = NULL;
                r->availableMoves[i] = moves[i];
            }
            else
                r->availableMoves[i] = (Vec2_c){-1, -1};
        }
        r->childrenAmt = numMoves;
        r->link = linkMove;
        for(int i = 0; i < 4; i++)
            r->value[i] = nINF;
        r->score[0] = nINF; 
        r->score[1] = nINF;
    }
    return r;
}

void freeGameNode(GameNode* node)
{
    if(node == NULL)
        return;
    for(int i = 0; i < node->childrenAmt; i++)
    {
        if(node->children[i] != NULL)
            freeGameNode(node->children[i]);
    }
    if(node->childrenAmt > 0 && node->children != NULL)
        free(node->children);
    free(node);
}

int nodeIsEqualToState(struct ai_boardInter boardInter, GameNode* node, GameState* state)
{
    if(node->childrenAmt != boardInter.getAvailableTileSize(state))
        return 0;
    for(int i = 0; i < 64; i++)
    {
        if(node->board[i/8][i%8] != boardInter.getTile(state, i/8, i%8))
            return 0;
    }
    return 1;
}

void freeGameNodeUpToChild(GameNode* node, GameNode* child)
{
    if(node == NULL)
        return;
    if(node == child)
        return;
    for(int i = 0; i < node->childrenAmt; i++)
    {
        if(node->children[i] != NULL)
            freeGameNodeUpToChild(node->children[i], child);
    }
    if(node->childrenAmt > 0)
        free(node->children);
    free(node);
}

void freeGameNodeUpTo(struct ai_boardInter boardInter, GameNode* node, GameState* find, GameNode** output)
{
    if(node == NULL)
        return;
    if(nodeIsEqualToState(boardInter, node, find))
    {
        *output = node;
        return;
    }
    for(int i = 0; i < node->childrenAmt; i++)
    {
        if(node->children[i] != NULL)
            freeGameNodeUpTo(boardInter, node->children[i], find, output);
    }
    if(node->childrenAmt > 0)
        free(node->children);
    free(node);
}

const float stabWeights[8][8] = {
    { 4, -3,  2,  2,  2,  2, -3,  4},
    {-3, -4, -1, -1, -1, -1, -4, -3},
    { 2, -1,  1,  0,  0,  1, -1,  2},
    { 2, -1,  0,  1,  1,  0, -1,  2},
    { 2, -1,  0,  1,  1,  0, -1,  2},
    { 2, -1,  1,  0,  0,  1, -1,  2},
    { 4, -3,  2,  2,  2,  2, -3,  4},
    {-3, -4, -1, -1, -1, -1, -4, -3}
} ;

// https://courses.cs.washington.edu/courses/cse573/04au/Project/mini1/RUSSIA/Final_Paper.pdf
float getHeuristicScore(struct ai_boardInter boardInter, GameNode* node, TileType forPlayer)
{
    if(node->score[forPlayer] != nINF)
        return node->score[forPlayer];
    float coinParity = 0;
    {
        int score1 = 0;
        int score2 = 0;
        for(int i = 0; i < 64; i++)
        {
            if(node->board[i/8][i%8] == forPlayer)
                score1++;
            else if(node->board[i/8][i%8] == (forPlayer+1)%2)
                score2++;
        }
        coinParity = 100.0f*(((float)(score1-score2))/((float)(score1+score2)));
    }
    float actualMobility = 0;
    {
        int mobility1 = node->childrenAmt;
        int mobility2 = 0;
        for(int i = 0; i < 64; i++)
        {
            if(node->board[i/8][i%8] == T_EMPTY)
                continue;
            Neighbor n[8];
            boardInter.getValidNeighbors(node->board, (forPlayer+1)%2, n, i/8, i%8);
            for(int j = 0; j < 8; j++)
            {
                if(n[j].posX != -1)
                {
                    mobility2++;
                    break;
                }
            }
        }
        if(mobility1+mobility2 != 0)
            actualMobility = 100.0f*(((float)(mobility1-mobility2))/((float)(mobility1+mobility2)));
    }
    float stability = 0;
    {
        int stabScore1 = 0;
        int stabScore2 = 0;
        for(int i = 0; i < 64; i++)
        {
            if(node->board[i/8][i%8] == forPlayer)
                stabScore1 += stabWeights[i/8][i%8];
            else if(node->board[i/8][i%8] == (forPlayer+1)%2)
                stabScore2 += stabWeights[i/8][i%8];
            if(stabScore1+stabScore2 != 0)
                stability = 100.0f*(((float)(stabScore1-stabScore2))/((float)(stabScore1+stabScore2)));
        }
    }
    float corner = 0;
    {
        int owned1 = 0;
        int owned2 = 0;
        if(node->board[0][0] == forPlayer)
            owned1++;
        else if(node->board[0][0] == (forPlayer+1)%2)
            owned2++;
        if(node->board[7][0] == forPlayer)
            owned1++;
        else if(node->board[7][0] == (forPlayer+1)%2)
            owned2++;
        if(node->board[7][7] == forPlayer)
            owned1++;
        else if(node->board[7][7] == (forPlayer+1)%2)
            owned2++;
        if(node->board[0][7] == forPlayer)
            owned1++;
        else if(node->board[0][7] == (forPlayer+1)%2)
            owned2++;
        if(owned1+owned2 != 0)
            corner = 100.0f*(((float)(owned1-owned2))/((float)(owned1+owned2)));
    }
    float result = 
        corner * 30
        + actualMobility * 10
        + coinParity * 25
        + stability * 35;
    
    node->score[forPlayer] = result;

    return result;
}
