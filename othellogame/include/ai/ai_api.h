/**
 * @file ai_api.h
 * @author Peyrille Benjamin, Rogalle Quentin
 * @brief Description des fonctions utilisables par les IA.
 */

#pragma once

#include "board.h"
#include "gameState.h"

struct ai_boardInter
{
    GameStatus (*getStatus)(GameState* state);

    TileType (*getTile)(GameState* state, char posY, char posX);
    TileType (*getFutureTile)(GameState* state, char posY, char posX);

    void (*getBoard)(GameState* state, TileType board[8][8]);
    void (*getFutureBoard)(GameState* state, TileType board[8][8]);

    void (*getNeighbors)(Neighbor neighbors[], char posY, char posX);
    void (*getValidNeighbors)(TileType board[8][8], TileType currentPlayer, 
        Neighbor neighbors[], char posY, char posX);


    char* (*getPlayerType)(GameState* state, char playerNum);
    GameAction (*getFutureAction)(GameState* state);

    int (*getAvailableTileSize)(GameState* state);
    Vec2_c (*getAvailableTile)(GameState* state, int index);
};

struct ai_actionInter
{
    int (*placeStone)(GameState* state, char posY, char posX);
    int (*cancelAction)(GameState* state);
};

struct ai_inter
{
    struct ai_boardInter boardInter;
    struct ai_actionInter actionInter;
};
