/**
 * @file board.h
 * @author Peyrille Benjamin, Rogalle Quentin
 * @brief Fonctions permettant de manipuler l'othellier
 */

#pragma once

#include "gameState.h"

// Prototype de la structure de jeu
typedef struct sGameState GameState;

#pragma region : Structures
/**
 * @brief Structure représentant le voisin d'une case
 * (posX, posY) représente la position de la case
 * (relX, relY) représente la position relative de la case (par rapport à la case dont elle est voisine).
 */
typedef struct
{
    int posY;
    int posX;
    int relY;
    int relX;
} Neighbor;

// Le voisin vide, utile pour la détection d'erreur
extern const Neighbor EMPTY_NEIGHBOR;
#pragma endregion : Structures

/**
 * @brief Récupère les voisins valides de la case à la position (posX, posY)
 * Un voisin valide par rapport à currentPlayer
 *  est un voisin qui sera retourné si le joueur currentPlayer place sa pierre ici.
 * @param board Plateau de jeu
 * @param currentPlayer Joueur qui va jouer le coup
 * @param neighbors Tableau qui sera rempli des voisins valides
 * @param posY Position Y de la case
 * @param posX Position X de la case
 */
void getValidNeighbors(TileType board[8][8], TileType currentPlayer, 
    /*@out@*/ Neighbor* neighbors, char posY, char posX);

/**
 * @brief Récupère les voisins de la case à la position (posX, posY)
 * @param neighbors Tableau qui sera rempli des voisins
 * @param posY Position Y de la case
 * @param posX Position X de la case
 */
void getNeighbors(/*@out@*/ Neighbor* neighbors, char posY, char posX);


/**
 * @brief Réalise le coup définitivement et passe le tour
 * @param state Structure de jeu
 * @return char 
 */
char prepareNextTurn(GameState* state);

/**
 * @brief Vérifie que le coup (posX, posY) est valide, si oui, réalise le coup
 * @param state Structure de jeu
 * @param posY Position Y du coup
 * @param posX Position X du coup
 * @return int 1 si le coup a réussi, 0 sinon
 */
int placeStone(GameState* state, char posY, char posX);
