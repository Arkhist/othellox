/**
 * @file termInterface.h
 * @author Peyrille Benjamin, Rogalle Quentin
 * @brief Déclare l'interface terminal
 */

#pragma once

#include "interface.h"
#include "game.h"


extern struct interface terminalInterface;