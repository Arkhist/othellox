/**
 * @file interface.h
 * @author Peyrille Benjamin, Rogalle Quentin
 * @brief Déclare la structure d'une interface graphique
 */

#pragma once

#include "game.h"

/**
 * @brief Dossier contenant les IA
 */
extern char aiDirectory[256];

/**
 * @brief Structure définissant une interface graphique
 */
struct interface
{
    int (*init)();
    void (*updateMenu)(/*@out@*/ ProgramState* state, /*@out@*/ int* logGame, /*@out@*/ char** p1, /*@out@*/ char** p2);
    void (*drawMenu)(ProgramState state);

    PlayerAction (*getGameInput)(GameState* gState);
    void (*drawGame)(GameState* gState);
    void (*destroy)();
};
