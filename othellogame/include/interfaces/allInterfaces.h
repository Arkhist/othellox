/**
 * @file allInterfaces.h
 * @author Peyrille Benjamin, Rogalle Quentin
 * @brief Include unique permettant d'inclure toutes les interfaces
 */

#pragma once

#include "termInterface.h"
#include "noDispInterface.h"
#include "curInterface.h"