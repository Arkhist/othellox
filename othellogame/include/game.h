/**
 * @file game.h
 * @author Peyrille Benjamin, Rogalle Quentin
 * @brief Fonctions permettant de manipuler le jeu
 */

#pragma once

#include "gameState.h"
#include "board.h"

// Prototype de l'interface graphique (structure incomplète)
struct interface;

#pragma region : Enumerations
/**
 * @brief Énumération décrivant l'état du jeu
 */
typedef enum
{
    MENU,
    GAME_START,
    GAME,
    GAME_EXIT,
    EXIT,
    EXITING
} ProgramState;

/**
 * @brief Énumération décrivant les différentes actions que peut faire le joueur
 */
typedef enum
{
    PA_PLACE,
    PA_CONFIRM,
    PA_CANCEL,
    PA_GIVEUP,
    PA_OTHER
} PlayerActionTypes;

/**
 * @brief Structure représentant une action que le joueur peut réaliser
 */
typedef struct
{
    PlayerActionTypes type;
    int x;
    int y;
} PlayerAction;

extern double timeoutai;

#pragma endregion : Enumerations

/**
 * @brief Permet de lancer le jeu et récupérer la structure associée au jeu
 * @param pState Etat du programme actuel
 * @param logGame 1 si le jeu doit enregistrer la partie, 0 sinon
 * @param player1 Nom du joueur 1
 * @param player2 Nom du joueur 2
 * @return GameState* Structure de jeu si réussi, NULL sinon
 */
GameState* initGame(/*@out@*/ ProgramState* pState, int logGame, char* player1, char* player2);


/**
 * @brief Fait la mise à jour des éléments du jeu et lance les fonctions d'intéraction de l'interface graphique
 * @param pState Etat du programme actuel
 * @param state Structure de jeu
 * @param interface Interface graphique
 */
void updateGame(/*@out@*/ ProgramState* pState, GameState* state, struct interface* interface);

/**
 * @brief Fait l'affichage du jeu
 * @param state Structure de jeu
 * @param interface Interface graphique
 */
void drawGame(GameState* state, struct interface* interface);

/**
 * @brief Met à jour le tour de jeu actuel et vérifie la fin de jeu
 * @param state Structure de jeu
 */
void updateTurn(GameState* state);


/**
 * @brief Éxécute les interactions du joueur
 * @param state Structure de jeu
 * @param action Action à réaliser
 */
void executePlayer(GameState* state, PlayerAction action);

/**
 * @brief Éxécute l'IA dont c'est le tour et vérifie la légalité de ses coups
 * @param state Structure de jeu
 */
void executeAI(GameState* state);


/**
 * @brief Confirme le tour du joueur actuel
 * @param state Structure de jeu
 * @return int 1 si l'action a été réussie, 0 sinon
 */
int confirmAction(GameState* state);

/**
 * @brief Annule le tour du joueur actuel
 * @param state Structure de jeu
 * @return int 
 */
int cancelAction(GameState* state);