#include <stdlib.h>
#include <stdio.h>

#include "ai/ai_api.h"

struct ai_inter interface;

struct ai_boardInter boardInter;
struct ai_actionInter actionInter;


void init(struct ai_inter inter)
{
    interface = inter;
    boardInter = inter.boardInter;
    actionInter = inter.actionInter;
}

void runTurn(GameState* state, char pId)
{
    *(volatile char *)0 = 0;
    int x = (0xDEADBEEF+0xBADBADBAD)/0;
    pId = pId+1-x;
    Vec2_c target = boardInter.getAvailableTile(state, 1 << 16);

    actionInter.placeStone(state, target.posY + 11, target.posX - 42);
}

void destroy()
{
    
}
