#include "game_test.h"

#include "board_test.h"

#include "termUtil.h"

#include "game.h"
#include "gameState.h"


int runInitTests()
{
    printf("Creating a test game...");
    GameState* testState = createGameState(0, "human", "human");
    if(testState == NULL)
    {
        CLEAR_LINE();
        printf("\t%s%sImpossible to create a test game%s%s\n", 
                CSI, RED_COLOR, CSI, WHITE_COLOR);
        return 0;
    }
    else
        CLEAR_LINE();
    
    /*setTile(testState, 3, 3, T_P2);
    setTile(testState, 4, 3, T_P1);
    setTile(testState, 4, 4, T_P2);
    setTile(testState, 3, 4, T_P1);*/
    prepareNextTurn(testState);

    if(getAvailableTileSize(testState) != 4)
    {
        printf("\tAmount of tiles playable at start %s%sis different%s%s than 4 : %d\n", 
                CSI, RED_COLOR, CSI, WHITE_COLOR, getAvailableTileSize(testState));
        return 0;
    }
    
    printf("\tGame initialization tests : %s%sSUCCESS%s%s\n", CSI, GREEN_COLOR, CSI, WHITE_COLOR);
    destroyGameState(testState);

    return 1;
}

int runTurnTests()
{
    GameState* testState = createGameState(0, "human", "human");

    prepareNextTurn(testState);

    int testNum = 3;
    struct BoardState states[testNum];
    Vec2_c moves[testNum];
    int available[testNum];

    states[0] = (struct BoardState){
        5,
        {3,     4,      4,      3,      2},
        {3,     3,      4,      4,      3},
        {T_P1,  T_P1,   T_P2,   T_P1,   T_P1}
    };
    moves[0] = (Vec2_c){2, 3};
    available[0] = 4;

    states[1] = (struct BoardState){
        6,
        {3,     4,      4,      3,      2,      4},
        {3,     3,      4,      4,      3,      2},
        {T_P1,  T_P2,   T_P2,   T_P1,   T_P1,   T_P2}
    };
    moves[1] = (Vec2_c){4, 2};
    available[1] = 3;

    states[2] = (struct BoardState){
        7,
        {3,     4,      4,      3,      2,      4,      5},
        {3,     3,      4,      4,      3,      2,      1},
        {T_P1,  T_P2,   T_P2,   T_P1,   T_P1,   T_P1,   T_P1}
    };
    moves[2] = (Vec2_c){5, 1};
    available[2] = 5;

    int success = 1;

    for(int i = 0; i < testNum; i++)
    {
        if(getAvailableTileSize(testState) != available[i])
        {
            printf("\tPlacing valid stone available amount %d/%d : %s%sFAILED%s%s\n", i+1, testNum, CSI, RED_COLOR, CSI, WHITE_COLOR);
            success = 0;
            break;
        }
        if(!placeStone(testState, moves[i].posY, moves[i].posX))
        {
            printf("\tPlacing valid stone validity %d/%d : %s%sFAILED%s%s\n", i+1, testNum, CSI, RED_COLOR, CSI, WHITE_COLOR);
            success = 0;
            break;
        }
        for(int j = 0; j < states[i].tileAmount; j++)
            if(getFutureTile(testState, states[i].posYs[j], states[i].posXs[j]) != states[i].tiles[j])
            {
                printf("\tPlacing valid stone board state %d/%d : %s%sFAILED%s%s\n", i+1, testNum, CSI, RED_COLOR, CSI, WHITE_COLOR);
                success = 0;
                break;
            }
        if(!success)
            break;
        confirmAction(testState);
        updateTurn(testState);
    }

    if(success)
        printf("\t%d Stone turn tests : %s%sSUCCESS%s%s\n", testNum, CSI, GREEN_COLOR, CSI, WHITE_COLOR);

    destroyGameState(testState);

    return success;
}

int runGameTests()
{
    printf("Starting game tests...\n");
    if(!runInitTests())
        return GAME_FAILED;
    if(!runTurnTests())
        return GAME_FAILED;
    printf("Game tests passed.\n");
    return 0;
}