#include "board_test.h"
#include "game_test.h"
#include "ai_test.h"

#include <stdio.h>


int main(int argc, char** argv)
{
    printf("\n");

    int status = 0;

    status |= runBoardTests();

    if(!(status & BOARD_FAILED))
        status |= runGameTests();
    
    if(!(status & (GAME_FAILED | BOARD_FAILED)))
        status |= runAItests();

    printf("\n");

    return status;
}