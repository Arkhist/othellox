#include "ai_test.h"

#include "termUtil.h"

#include "game.h"
#include "gameState.h"


int runInvalidAIs()
{
    GameState* testState = createGameState(0, "./unit_tests/impossible-ai", "human");
    if(testState != NULL)
    {
        CLEAR_LINE();
        printf("\tImpossible AI test : %s%sFAILED%s%s\n", 
                CSI, RED_COLOR, CSI, WHITE_COLOR);
        destroyGameState(testState);
        return 0;
    }

    testState = createGameState(0, "./unit_tests/verywrong-ai", "human");
    if(testState != NULL)
    {
        CLEAR_LINE();
        printf("\tIncomplete AI test : %s%sFAILED%s%s\n", 
                CSI, RED_COLOR, CSI, WHITE_COLOR);
        destroyGameState(testState);
        return 0;
    }

    testState = createGameState(0, "./unit_tests/wrong-ai", "human");
    if(testState == NULL)
    {
        CLEAR_LINE();
        printf("\tComplete but wrong AI test loading : %s%sFAILED%s%s\n", 
                CSI, RED_COLOR, CSI, WHITE_COLOR);
        return 0;
    }
    prepareNextTurn(testState);
    executeAI(testState);
    if(getStatus(testState) != GAMEOVER)
    {
        CLEAR_LINE();
        printf("\tComplete but wrong AI play test : %s%sFAILED%s%s\n", 
                CSI, RED_COLOR, CSI, WHITE_COLOR);
        destroyGameState(testState);
        return 0;
    }
    destroyGameState(testState);

    printf("\tInvalid AI tests : %s%sSUCCESS%s%s\n", CSI, GREEN_COLOR, CSI, WHITE_COLOR);

    return 1;
}

int runValidAI()
{
    GameState* testState = createGameState(0, "./unit_tests/valid-ai", "human");
    if(testState == NULL)
    {
        CLEAR_LINE();
        printf("\tValid AI loading : %s%sFAILED%s%s\n", 
                CSI, RED_COLOR, CSI, WHITE_COLOR);
        return 0;
    }
    prepareNextTurn(testState);
    executeAI(testState);
    if(getStatus(testState) == GAMEOVER)
    {
        CLEAR_LINE();
        printf("\tValid AI play test : %s%sFAILED%s%s\n", 
                CSI, RED_COLOR, CSI, WHITE_COLOR);
        destroyGameState(testState);
        return 0;
    }
    destroyGameState(testState);

    printf("\tValid AI play test : %s%sSUCCESS%s%s\n", CSI, GREEN_COLOR, CSI, WHITE_COLOR);
    return 1;
}

int runAItests()
{
    printf("Starting AI tests...\n");
    if(!runInvalidAIs())
        return AI_FAILED;
    if(!runValidAI())
        return AI_FAILED;
    printf("AI tests passed.\n");
    return 0;
}