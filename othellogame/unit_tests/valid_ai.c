#include <stdlib.h>
#include <stdio.h>

#include "ai/ai_api.h"

struct ai_inter interface;

struct ai_boardInter boardInter;
struct ai_actionInter actionInter;


void init(struct ai_inter inter)
{
    interface = inter;
    boardInter = inter.boardInter;
    actionInter = inter.actionInter;
}

void runTurn(GameState* state, char pId)
{    
    pId = pId+1-1;
    Vec2_c target = boardInter.getAvailableTile(state, 0);

    actionInter.placeStone(state, target.posY, target.posX);
}

void destroy()
{
    
}
