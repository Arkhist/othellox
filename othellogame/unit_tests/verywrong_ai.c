#include <stdlib.h>
#include <stdio.h>

#include "ai/ai_api.h"

struct ai_inter interface;

struct ai_boardInter boardInter;
struct ai_actionInter actionInter;


void init(struct ai_inter inter)
{
    interface = inter;
    boardInter = inter.boardInter;
    actionInter = inter.actionInter;
}

void destroy()
{
    
}
