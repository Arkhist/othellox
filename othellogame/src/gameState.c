/**
 * @file gameState.c
 * @author Peyrille Benjamin, Rogalle Quentin
 * @brief Déclaration des structures du jeu et fonctions de manipulation de celles-ci.
 */

#include "game.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "ai/ai_handler.h"
#include "logger.h"

struct sGameState
{
    GameStatus status;
    TileType board[8][8];
    GameAction futureAction;
    TileType futureBoard[8][8];
    char playerTypes[2][256];
    struct ai_access* aiHandlers[2];
    
    int numAvailableTiles;
    Vec2_c availableTiles[64];

    FILE* logFile;

    int finalScore[2];
    char gameOverMessage[512];
    TileType winner;
};

GameState* createGameState(int logGame, char* player1, char* player2)
{
    GameState* r = (GameState*)calloc(1, sizeof(GameState));
    if(r != NULL)
    {
        // Initialisation du plateau de jeu à vide.
        for(int i = 0; i < 64; i++)
        {
            r->board[i/8][i%8] = T_EMPTY;
            r->futureBoard[i/8][i%8] = T_EMPTY;
        }
        for(int i = 0; i < 2; i++)
        {
            strcpy(r->playerTypes[i], (i == 0) ? player1 : player2);
            
            // Si le joueur n'est pas humain, on créé l'IA associée.
            if (strcmp(r->playerTypes[i],"human") != 0) 
            {
                r->aiHandlers[i] = makeAiAccess(i, r->playerTypes[i]);
                // Si l'IA n'a pas pu être créé
                if(r->aiHandlers[i] == NULL)
                {
                    for(;i >= 0; i--)
                        if(r->aiHandlers[i] != NULL)
                            destroyAiAccess(r->aiHandlers[i]);
                    free(r);
                    return NULL;
                }
            }
        }

        setTile(r, 3, 3, T_P2);
        setTile(r, 4, 3, T_P1);
        setTile(r, 4, 4, T_P2);
        setTile(r, 3, 4, T_P1);
        
        r->futureAction = (GameAction){-1, -1, -1};
        r->status = INIT;

        if(logGame)
            r->logFile = createLogFile();
        else
            r->logFile = NULL;
        
        for(int i = 0; i < 256; i++)
            r->gameOverMessage[i] = 0;
    }
    return r;
}

void destroyGameState(GameState* state)
{
    if (state->aiHandlers[0] != NULL)
        destroyAiAccess(state->aiHandlers[0]);
    // Si l'IA est ouverte deux fois, on ne la ferme qu'une seule fois
    if (state->aiHandlers[1] != NULL && strcmp(state->playerTypes[0], state->playerTypes[1]))
        destroyAiAccess(state->aiHandlers[1]);
    if(state->logFile != NULL)
    {
        fclose(state->logFile);
    }
    free(state);
}

#pragma region : GameState Getters

struct ai_access* getAiHandler(GameState* state, int index)
{
    return state->aiHandlers[index];
}

GameStatus getStatus(GameState* state)
{
    return state->status;
}

GameAction getFutureAction(GameState* state)
{
    return state->futureAction;
}

TileType getTile(GameState* state, char posY, char posX)
{
    return state->board[posY][posX];
}

TileType getFutureTile(GameState* state, char posY, char posX)
{
    return state->futureBoard[posY][posX];
}

void getBoard(GameState* state, TileType board[8][8])
{
    for(int i = 0; i < 64; i++)
        board[i/8][i%8] = state->board[i/8][i%8];
}
void getFutureBoard(GameState* state, TileType board[8][8])
{
    for(int i = 0; i < 64; i++)
        board[i/8][i%8] = state->futureBoard[i/8][i%8];
}

char* getPlayerType(GameState* state, char playerNum)
{
    return state->playerTypes[playerNum];
}

int getAvailableTileSize(GameState* state)
{
    return state->numAvailableTiles;
}

Vec2_c getAvailableTile(GameState* state, int index)
{
    return state->availableTiles[index];
}

FILE* getLogFile(GameState* state)
{
    return state->logFile;
}

char* getGameOverMessage(GameState* state)
{
    return state->gameOverMessage;
}

void getFinalScore(GameState* state, int* s1, int* s2)
{
    *s1 = state->finalScore[0];
    *s2 = state->finalScore[1];
}

TileType getWinner(GameState* state)
{
    return state->winner;
}
#pragma endregion : GameStatus Getters

#pragma region : GameState Setters
void setFutureAction(GameState* state, char posY, char posX, TileType player)
{
    state->futureAction.posY = posY;
    state->futureAction.posX = posX;
    state->futureAction.player = player;
}

void setStatus(GameState* state, GameStatus status)
{
    state->status = status;
}

void setTile(GameState* state, char posY, char posX, TileType Tilestate)
{
    state->board[posY][posX] = Tilestate;
    state->futureBoard[posY][posX] = Tilestate;
}

void setFutureTile(GameState* state, char posY, char posX, TileType Tilestate)
{
    state->futureBoard[posY][posX] = Tilestate;
}

void setAvailableTile(GameState* state, Vec2_c availableTile[], int size)
{
    for(int i = 0;i < size; i++)
        state->availableTiles[i] = availableTile[i];
    state->numAvailableTiles = size;
}

void resetAvailableTiles(GameState* state)
{
    state->numAvailableTiles = 0;
}

void setGameOverMessage(GameState* state, char message[512])
{
    state->finalScore[0] = 0;
    state->finalScore[1] = 0;
    for(int i = 0; i<64; i++)
        if (getTile(state, i/8, i%8) != T_EMPTY)
        {
            if(getTile(state, i/8, i%8) == T_P1)
                state->finalScore[0]++;
            else
                state->finalScore[1]++;
        }
    strcpy(state->gameOverMessage, message);
}

void setWinner(GameState* state, TileType winner)
{
    state->winner = winner;
}
#pragma endregion : GameState Setters