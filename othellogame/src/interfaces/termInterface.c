/**
 * @file termInterface.c
 * @author Peyrille Benjamin, Rogalle Quentin
 * @brief Interface Terminal
 */

#include "interfaces/termInterface.h"

#include <stdio.h>
#include <string.h>

#define CSI "\33["

static const char* CLR_SCREEN = "2J";
static const char* RED_COLOR = "31m";
static const char* GREEN_COLOR = "32m";
static const char* WHITE_COLOR = "37m";

static int initTTY();
static void updateMenuTTY(ProgramState* state, int* logGame, char** p1, char** p2);
static void drawMenuTTY(ProgramState state);

static PlayerAction getGameInputTTY(GameState* gState);
static void drawGameTTY(GameState* gState);
static void destroyTTY();

struct interface terminalInterface =
{
    initTTY,
    updateMenuTTY,
    drawMenuTTY,
    getGameInputTTY,
    drawGameTTY,
    destroyTTY
};

static int initTTY()
{
    return 1;
}

static void updateMenuTTY(ProgramState* state, int* logGame, char** p1, char** p2)
{
    printf("%s%s\n", CSI, CLR_SCREEN);
    char in;
    printf("Do you want to (s)tart a game ? Or (q)uit ? ");
    in = getc(stdin);
    //scanf(" %c", &in);
    if(in == 's')
    {
        printf("Player 1 (human/AI-ai) ? ");
        scanf(" %64s", *p1);
        printf("Player 2 (human/AI-ai) ? ");
        scanf(" %64s", *p2);
        *state = GAME_START;
    }
    else
    {
        *state = EXITING;
    }
}

static void drawMenuTTY(ProgramState state)
{

}

static PlayerAction getGameInputTTY(GameState* gState)
{
    PlayerAction action = (PlayerAction){PA_OTHER, -1, -1};
    switch(getStatus(gState))
    {
        case PLAYER1:
        case PLAYER2:
        {
            const char* color = ((getStatus(gState) == PLAYER1) ? RED_COLOR : GREEN_COLOR);
            printf("It's %s%splayer %d%s%s turn.\n", CSI, color, (getStatus(gState) == PLAYER1) ? 1 : 2, CSI, WHITE_COLOR);
            if((getStatus(gState) == PLAYER1 && !strcmp(getPlayerType(gState, 0), "human"))
                || (getStatus(gState) == PLAYER2 && !strcmp(getPlayerType(gState, 1), "human")))
            {
                char input;
                if(getFutureAction(gState).posX == -1)
                {
                    printf("Play- ");
                    input = 'p';
                }
                else
                {
                    printf("Confirm(c), Undo(u)");
                    printf(" : ");
                    input = getc(stdin);
                }
                switch (input)
                {
                    case 'p':
                        if(getFutureAction(gState).posX != -1)
                            break;
                        action.type = PA_PLACE;
                        printf("X Y ? ");
                        action.x = getc(stdin) - '0';
                        getc(stdin);
                        action.y = getc(stdin) - '0';
                        //scanf("%d %d", &action.x, &action.y);
                        action.x--;
                        action.y--;
                        break;
                    case 'c':
                        action.type = PA_CONFIRM;
                        break;
                    case 'u':
                        action.type = PA_CANCEL;
                        break;
                    default:
                        action.type = PA_OTHER;
                        break;
                }
            }
            else
            {
                char in;
                printf("Press a key to continue... ");
                in = getc(stdin);
            }
            return action;
        }
        case GAMEOVER:
        {
            int score[2] = {0};
            for(int i = 0; i < 64; i++)
                score[getTile(gState, i/8, i%8)]++;
            printf("Scores : %d - %d\n", score[0], score[1]);
            printf("Press a key to continue.");
            
            char tmp;
            tmp = getc(stdin);
            setStatus(gState, CLEANUP);
        }
        default:
            break;
    }
    return action;
}

static void drawGameTTY(GameState* gState)
{   
    printf("%s%s\n", CSI, CLR_SCREEN);
    for(int i = 0; i < 8; i++)
    {
        if(i == 0)
            printf("Y\\X");
        printf("  %d ", i+1);
    }
    printf("\n");
    for(int i = 0; i < 8; i++)
    {
        if(i == 0)
            printf("   \u250f");
        printf("\u2501\u2501\u2501");
        if(i != 7)
            printf("\u2533");
        else
            printf("\u2513\n");
    }
    for(int i = 0; i < 8; i++)
    {
        // Draw Characters
        for(int j = 0; j < 8; j++)
        {
            char cur = ' ';
            const char* curColor = WHITE_COLOR;
            if(getFutureAction(gState).posX == -1)
            {
                for(int k = getAvailableTileSize(gState)-1; k >= 0; k--)
                if(getAvailableTile(gState, k).posX == j && getAvailableTile(gState, k).posY == i)
                {
                    cur = 'X';
                    break;
                }
            }
            switch(getFutureTile(gState, i, j))
            {
                case T_P1:
                    cur = 'O';
                    curColor = RED_COLOR;
                    break;
                case T_P2:
                    cur = 'O';
                    curColor = GREEN_COLOR;
                    break;
                default:
                    break;
            }
            if(j == 0)
                printf(" %d ", i+1);
            printf("\u2503 %s%s%c%s%s ", CSI, curColor, cur, CSI, WHITE_COLOR);
        }
        printf("\u2503\n");

        for(int j = 0; j < 8; j++)
        {
            if(i != 7)
            {
                if(j == 0)
                    printf("   \u2523");
                printf("\u2501\u2501\u2501");
                if(j != 7)
                    printf("\u254b");
                else
                    printf("\u252b\n");
            }
            else
            {
                if(j == 0)
                    printf("   \u2517");
                printf("\u2501\u2501\u2501");
                if(j != 7)
                    printf("\u253b");
                else
                    printf("\u251b\n");
            }
        }
    }
}

static void destroyTTY()
{
    
}
