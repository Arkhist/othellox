/**
 * @file noDispInterface.c
 * @author Peyrille Benjamin, Rogalle Quentin
 * @brief Interface Sans Affichage
 */

#include "interfaces/noDispInterface.h"

#include <stdio.h>

static int initND();
static void updateMenuND(ProgramState* state, int* logGame, char** p1, char** p2);
static void drawMenuND(ProgramState state);

static PlayerAction getGameInputND(GameState* gState);
static void drawGameND(GameState* gState);
static void destroyND();

struct interface noDisplayInterface = 
{
    initND,
    updateMenuND,
    drawMenuND,
    getGameInputND,
    drawGameND,
    destroyND
};

static int initND()
{
    return 1;
}

static void updateMenuND(ProgramState* state, int* logGame, char** p1, char** p2)
{
    *state = EXITING;
}

static void drawMenuND(ProgramState state)
{

}

static PlayerAction getGameInputND(GameState* gState)
{
    if(getStatus(gState) == GAMEOVER)
    {
        int s1, s2;
        getFinalScore(gState, &s1, &s2);
        
        printf("%d - %d %d\n", getWinner(gState)+1, s1, s2);
        printf("%s\n", getGameOverMessage(gState));
        setStatus(gState, CLEANUP);
    }
    return (PlayerAction){PA_GIVEUP, -1, -1};
}

static void drawGameND(GameState* gState)
{

}

static void destroyND()
{

}