#ifndef WINDOWS
#define _GNU_SOURCE
#include <link.h>
#endif
#include <dlfcn.h>

#include <stdlib.h>

#include "ai/ai_handler.h"
#include "ai/ai_api.h"

#include "board.h"
#include "game.h"

// Ces structures sont envoyées à chaque IA.
const struct ai_boardInter ai_boardInter = {
    &getStatus,

    &getTile,
    &getFutureTile,

    &getBoard,
    &getFutureBoard,

    &getNeighbors,
    &getValidNeighbors,

    &getPlayerType,

    &getFutureAction,

    &getAvailableTileSize,
    &getAvailableTile
};

const struct ai_actionInter ai_actionInter = {
    &placeStone,
    &cancelAction
};


struct ai_access* makeAiAccess(char pId, char* ai_name)
{
    struct ai_access* access = (struct ai_access*)calloc(1, sizeof(struct ai_access));
    void (*ai_init)(struct ai_inter inter);
    void (*ai_runTurn)(GameState* state, char pId);
    void (*ai_destroy)();
    void* ai_handle;

    if(access == NULL)
        return NULL;

#ifndef WINDOWS
    // On ouvre le fichier IA, et on vérifie qu'il ne charge pas de symboles inconnus. (RTLD_NOW)
    ai_handle = dlopen(ai_name, RTLD_NOW | RTLD_LOCAL);

    if(ai_handle == NULL)
    {
        free(access);
        return NULL;
    }

    // On récupère les fonctions liées à l'IA
    ai_init = dlsym(ai_handle, "init");
    ai_runTurn = dlsym(ai_handle, "runTurn");
    ai_destroy = dlsym(ai_handle, "destroy");
    if(ai_init == NULL || ai_runTurn == NULL || ai_destroy == NULL)
    {
        free(access);
        dlclose(ai_handle);
        return NULL;
    }
#endif

    access->ai_handle = ai_handle;
    access->ai_init = ai_init;
    access->ai_runTurn = ai_runTurn;
    access->ai_destroy = ai_destroy;

    struct ai_inter ai_interface = {
        ai_boardInter,
        ai_actionInter
    };

    // On initialise l'IA.
    ai_init(ai_interface);

    return access;
}

void destroyAiAccess(struct ai_access* access)
{
#ifndef WINDOWS
    dlclose(access->ai_handle);
#endif
    free(access);
}
