/**
 * @file game.c
 * @author Peyrille Benjamin, Rogalle Quentin
 * @brief Fonctions d'exécution du jeu
 */

#include "game.h"

#include "interfaces/interface.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define TIMEOUT 1.0
#define KINDNESS 0.2

#ifndef WINDOWS
#include <pthread.h>
#include <unistd.h>
#include <signal.h>
#endif

#include "ai/ai_handler.h"

#include "logger.h"

static volatile int ai_execComplete = 0;
static volatile int ai_crashed = 0;
double timeoutai;

GameState* initGame(ProgramState* pState, int logGame, char* player1, char* player2)
{
    char rPlayer1[256] = {0}, rPlayer2[256] = {0};
    if(player1 == NULL)
        strcpy(rPlayer1, "human");
    // Si le joueur est humain
    else if(!strcmp(player1, "human") || player1[0] == '/')
        strcpy(rPlayer1, player1);
    // Si le joueur n'est pas humain et n'est pas un chemin absolu
    else
    {
        strcpy(rPlayer1, "./");
        strcat(rPlayer1, player1);
    }


    if(player2 == NULL)
        strcpy(rPlayer2, "human");
    else if(!strcmp(player2, "human") || player2[0] == '/')
        strcpy(rPlayer2, player2);
    else
    {
        strcpy(rPlayer2, "./");
        strcat(rPlayer2, player2);
    }


    GameState* result = createGameState(logGame, rPlayer1, rPlayer2);
    if(result != NULL)
    {
        *pState = GAME;
        if(logGame)
            logStart(getLogFile(result), rPlayer1, rPlayer2);
    }
    else
        *pState = MENU;
    return result;
}


void updateGame(ProgramState* pState, GameState* state, struct interface* interface)
{
    PlayerAction action;
    if (getStatus(state) != INIT)
        action = interface->getGameInput(state);

    switch(getStatus(state))
    {
        case INIT:
            // Initialisation du plateau.
            prepareNextTurn(state);
            break;
        case PLAYER1:
            if(strcmp(getPlayerType(state, 0), "human") == 0)
                executePlayer(state, action);
            else
                executeAI(state);
            break;
        case PLAYER2:
            if(strcmp(getPlayerType(state, 1), "human") == 0)
                executePlayer(state, action);    
            else
                executeAI(state);
            break;
        case CHECK:
            updateTurn(state);
            break;
        case GAMEOVER:
            break;
        case CLEANUP:
            *pState = GAME_EXIT;
            break;
    }
}

void drawGame(GameState* state, struct interface* interface)
{
    interface->drawGame(state);
}

void executePlayer(GameState* state, PlayerAction action)
{
    char message[512];
    if (action.type == PA_CONFIRM)
        confirmAction(state);
    else if (action.type == PA_CANCEL)
        cancelAction(state);
    else if (action.type == PA_PLACE)
        placeStone(state, action.y, action.x);
    else if (action.type == PA_GIVEUP)
    {
        sprintf(message,"Player %d left the game.", ((getStatus(state) == PLAYER1) ? T_P1:T_P2) + 1);
        setGameOverMessage(state, message);
        setStatus(state, GAMEOVER);
        setWinner(state, ((getStatus(state) == PLAYER1) ? T_P2:T_P1));
    }
}

int confirmAction(GameState* state)
{
    if(getFutureAction(state).posX == -1)
    {
        TileType curPlayer = getStatus(state) == PLAYER1 ? T_P1 : T_P2;
        if(strcmp(getPlayerType(state, curPlayer), "human"))
        {
            logFailure(getLogFile(state), getPlayerType(state, curPlayer), curPlayer);
            
            char message[512] = {0};
            sprintf(message, "Player %d : %s, had a critical failure.", curPlayer+1, getPlayerType(state, curPlayer));
            setGameOverMessage(state, message);
            setStatus(state, GAMEOVER);
            setWinner(state, (curPlayer+1)%2);
        }
        return 0;
    }
    logTurn(getLogFile(state), getFutureAction(state));
    setStatus(state, CHECK);
    return 1;
}

int cancelAction(GameState* state)
{
    if(getFutureAction(state).posX == -1)
        return 0;
    setFutureAction(state, -1, -1, getFutureAction(state).player);
    for(int i = 0; i < 64; i++)
        setFutureTile(state, i/8, i%8, getTile(state, i/8, i%8));
    return 1;
}

void signalHandler(int sig)
{
    int val = 0;
    ai_crashed = 1;
#ifndef WINDOWS
    pthread_exit(&val);
#endif
}

void initSignal()
{
#ifndef WINDOWS
    struct sigaction sigHandler;
    sigHandler.sa_handler = signalHandler;
    sigemptyset(&sigHandler.sa_mask);
    sigHandler.sa_flags = 0;
    sigaction(SIGSEGV, &sigHandler, NULL);
    sigaction(SIGABRT, &sigHandler, NULL);
    sigaction(SIGILL, &sigHandler, NULL);
#endif
}

void disableSignal()
{
#ifndef WINDOWS
    signal(SIGSEGV, SIG_DFL);
    signal(SIGABRT, SIG_DFL);
    signal(SIGILL, SIG_DFL);
#endif
}

struct aiExecArgs
{
    struct ai_access* handler;
    GameState* state;
};

void* ai_execution(void* arg)
{
    initSignal();
    struct aiExecArgs args = *(struct aiExecArgs*)arg;
    args.handler->ai_runTurn(args.state, getStatus(args.state) == PLAYER1 ? T_P1 : T_P2);
    ai_execComplete = 1;
    return NULL;
}

void executeAI(GameState* state)
{
    struct ai_access* handler = NULL;
    if (getStatus(state) == PLAYER1)
        handler = getAiHandler(state, 0);
    else if (getStatus(state) == PLAYER2)
        handler = getAiHandler(state, 1);
    else
    {
        setStatus(state, CLEANUP);
        return;
    }
    ai_execComplete = 0;
    ai_crashed = 0;
    int threadReady;
#ifndef WINDOWS
    pthread_t aiThread;
    struct aiExecArgs args = (struct aiExecArgs){handler, state};
    threadReady = pthread_create(&aiThread, NULL, &ai_execution, (void*)&args);
#endif
    if(threadReady)
    {
        setStatus(state, CLEANUP);
        fprintf(stderr, "Impossible to open AI thread, returned : %d\n", threadReady);
    }
    double timeElapsed = 0;
#ifndef WINDOWS
    pthread_detach(aiThread);

    clock_t begin = clock();
    clock_t end = clock();
    timeElapsed = (double)(end-begin)/(double)CLOCKS_PER_SEC;
#endif
    while(timeElapsed < timeoutai + KINDNESS && !ai_execComplete && !ai_crashed)
    {
#ifndef WINDOWS
        struct timespec ts;
        ts.tv_sec = 1 / 1000;
        ts.tv_nsec = (1 % 1000) * 1000000;
        nanosleep(&ts, NULL);
        end = clock();
        timeElapsed = (double)(end-begin)/(double)CLOCKS_PER_SEC;
#endif
    }
    if(ai_crashed)
    {
        TileType curPlayer = getStatus(state) == PLAYER1 ? T_P1 : T_P2;
        logFailure(getLogFile(state), getPlayerType(state, curPlayer), curPlayer);
        char message[512] = {0};
        sprintf(message, "Player %d : %s, had a fatal error.", curPlayer+1, getPlayerType(state, curPlayer));
        setGameOverMessage(state, message);
        setStatus(state, GAMEOVER);
        setWinner(state, (curPlayer+1)%2);
    }
    else if(!ai_execComplete)
    {
#ifndef WINDOWS
        pthread_cancel(aiThread);
#endif
        TileType curPlayer = getStatus(state) == PLAYER1 ? T_P1 : T_P2;
        logTimeout(getLogFile(state), getPlayerType(state, curPlayer), curPlayer);
        char message[512] = {0};
        sprintf(message, "Player %d : %s, had a timeout (timeout val : %f).", curPlayer+1, getPlayerType(state, curPlayer), timeoutai);
        setGameOverMessage(state, message);
        setStatus(state, GAMEOVER);
        setWinner(state, (curPlayer+1)%2);
    }
    else
    {
        confirmAction(state);
    }
    disableSignal();
}

void updateTurn(GameState* state)
{
    char ready = prepareNextTurn(state);
    if(!ready)
    {
        ready = prepareNextTurn(state);
        if(!ready)
        {
            setStatus(state, GAMEOVER);

            int score[2] = {0};
            for(int i = 0; i < 64; i++)
                score[getTile(state, i/8, i%8)]++;

            logGameOver(getLogFile(state), getPlayerType(state, 0), score[0], getPlayerType(state, 1), score[1]);
            char message[512] = {0};
            if(score[0] > score[1])
            {
                sprintf(message, "Player 1 : %s, wins.", getPlayerType(state, 0));
                setWinner(state, T_P1);
            }
            else if(score[0] < score[1])
            {
                sprintf(message, "Player 2 : %s, wins.", getPlayerType(state, 1));
                setWinner(state, T_P2);
            }
            else
            {
                sprintf(message, "Tie.");
                setWinner(state, T_EMPTY);
            }
            setGameOverMessage(state, message);
        }
    }
}